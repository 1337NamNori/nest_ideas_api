import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthGuard } from 'src/shared/auth.guard';
import { CommentService } from './comment.service';
import { CommentDTO, CommentRO } from './dto/comment.dto';
import { UseGuards } from '@nestjs/common';

@Resolver()
export class CommentResolver {
  constructor(private commentService: CommentService) {}

  @Query('comment')
  async comment(@Args('id') id: string): Promise<CommentRO> {
    return await this.commentService.findOne(id);
  }

  @Mutation('editComment')
  @UseGuards(new AuthGuard())
  async editComment(
    @Args('id') id: string,
    @Args('text') text: string,
    @Context('user') user,
  ): Promise<CommentRO> {
    const data: CommentDTO = { text };
    const { id: userId } = user;
    return await this.commentService.updateComment(id, userId, data);
  }

  @Mutation('deleteComment')
  @UseGuards(new AuthGuard())
  async deleteComment(
    @Args('id') id: string,
    @Context('user') user
  ): Promise<CommentRO> {
    const { id: userId } = user;
    return await this.commentService.deleteComment(id, userId);
  }
}