import { Resolver, Query, Args, Mutation, Context } from '@nestjs/graphql';
import { IdeaService } from './idea.service';
import { IdeaDTO, IdeaRO } from './dto/idea.dto';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../shared/auth.guard';
import { CommentDTO } from '../comment/dto/comment.dto';

@Resolver()
export class IdeaResolver {
  constructor(private ideaService: IdeaService) {}

  @Query('ideas')
  async ideas(@Args('page') page = 1): Promise<IdeaRO[]> {
    const ideasPagination = await this.ideaService.findAllIdeas('/ideas', page);
    return ideasPagination.items;
  }

  @Query('idea')
  async idea(@Args('id') id: string): Promise<IdeaRO> {
    return await this.ideaService.showIdea(id);
  }

  @Mutation('createComment')
  @UseGuards(new AuthGuard())
  async createComment(
    @Args('id') id: string,
    @Args('text') text: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const data: CommentDTO = { text };
    const { id: userId } = user;
    return await this.ideaService.comment(id, userId, data);
  }

  @Mutation('createIdea')
  @UseGuards(new AuthGuard())
  async createIdea(
    @Args('idea') idea: string,
    @Args('description') description: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    const data: IdeaDTO = { idea, description };
    return await this.ideaService.createIdea(data, userId);
  }

  @Mutation('updateIdea')
  @UseGuards(new AuthGuard())
  async updateIdea(
    @Args('id') id: string,
    @Args('idea') idea: string,
    @Args('description') description: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    const data: IdeaDTO = { idea, description };
    return await this.ideaService.updateIdea(id, data, userId);
  }

  @Mutation('deleteIdea')
  @UseGuards(new AuthGuard())
  async deleteIdea(
    @Args('id') id: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    return await this.ideaService.deleteIdea(id, userId);
  }

  @Mutation('upvoteIdea')
  @UseGuards(new AuthGuard())
  async upvoteIdea(
    @Args('id') id: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    return await this.ideaService.upvotes(id, userId);
  }

  @Mutation('downvoteIdea')
  @UseGuards(new AuthGuard())
  async downvoteIdea(
    @Args('id') id: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    return await this.ideaService.downvotes(id, userId);
  }

  @Mutation('bookmarkIdea')
  @UseGuards(new AuthGuard())
  async bookmarkIdea(
    @Args('id') id: string,
    @Context('user') user,
  ): Promise<IdeaRO> {
    const { id: userId } = user;
    return await this.ideaService.bookmarks(id, userId);
  }
}
