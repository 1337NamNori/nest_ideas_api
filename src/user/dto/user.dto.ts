import { IsEmail, IsNotEmpty, IsString, Length } from "class-validator";
import { IdeaRO } from "src/idea/dto/idea.dto";

export class UserDTO {
    @IsNotEmpty()
    @IsString()
    username: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsString()
    @Length(8)
    password: string;
}

export class UserLoginDTO {
    @IsNotEmpty()
    @IsString()
    username: string;

    @IsNotEmpty()
    @IsString()
    @Length(8)
    password: string;
}

export interface UserRO {
    id: string;
    username: string;
    email: string;
    createdAt: Date;
    token?: string;
    ideas?: IdeaRO[];
    bookmarks?: IdeaRO[];
}