import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  ParseIntPipe,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { UserDTO, UserLoginDTO, UserRO } from './dto/user.dto';
import { UserService } from './user.service';
import { AuthGuard } from '../shared/auth.guard';
import { Pagination } from 'src/types/pagination.interface';
import { Request } from 'express';
import { User } from 'src/shared/user.decorator';
import { ValidationPipe } from 'src/shared/validation.pipe';

@Controller('api')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('users')
  @UseGuards(new AuthGuard())
  findAll(
      @Req() request: Request,
      @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
  ): Promise<Pagination<UserRO>> {
    return this.userService.findAllUsers(request.route.path, page);
  }

  @Post('auth/register')
  @UsePipes(new ValidationPipe())
  register(@Body() data: UserDTO): Promise<UserRO> {
    return this.userService.register(data);
  }

  @Post('auth/login')
  @UsePipes(new ValidationPipe())
  login(@Body() data: UserLoginDTO): Promise<UserRO> {
    return this.userService.login(data);
  }

  @Get('auth/user')
  @UseGuards(new AuthGuard())
  getUserInfo(@User('id') userId: string): Promise<UserRO> {
    return this.userService.getUserInfo(userId);
  }

  @Get('bookmarks')
  @UseGuards(new AuthGuard())
  getBookmarks(
    @User('id') userId: string,
  ): Promise<UserRO> {
    return this.userService.getMyBookmarks(userId);
  }
}
