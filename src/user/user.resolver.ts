import { Resolver, Query, Args, Mutation, Context } from '@nestjs/graphql';
import { UserService } from './user.service';
import { UserRO } from './dto/user.dto';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../shared/auth.guard';

@Resolver()
export class UserResolver {
  constructor(private userService: UserService) {}

  @Query('users')
  async users(@Args('page') page =1): Promise<UserRO[]> {
    console.log(page);
    const usersPagination = await this.userService.findAllUsers('/users', page);
    return usersPagination.items;
  }

  @Query('user')
  async user(@Args('id') id: string): Promise<UserRO> {
    console.log(id);
    return await this.userService.getUserInfo(id);
  }

  @Query('me')
  @UseGuards(new AuthGuard())
  async me(@Context('user') user): Promise<UserRO> {
    console.log(user);
    return await this.userService.getUserInfo(user.id);
  }

  @Mutation('login')
  async login(
    @Args('username') username: string,
    @Args('password') password: string,
  ): Promise<UserRO> {
    return await this.userService.login({username, password});
  }

  @Mutation('register')
  async register(
    @Args('username') username: string,
    @Args('email') email: string,
    @Args('password') password: string,
  ): Promise<UserRO> {
    return await this.userService.register({username, email, password});
  }
}
