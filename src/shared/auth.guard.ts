import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class AuthGuard implements CanActivate {
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (request) {
      AuthGuard.hasAuthHeader(request);
      request.user = await AuthGuard.validateToken(request.headers.authorization);

      return true;
    } else {
      const ctx: any = GqlExecutionContext.create(context).getContext();
      AuthGuard.hasAuthHeader(ctx);
      ctx.user = await AuthGuard.validateToken(ctx.headers.authorization);

      return true;
    }
  }

  private static hasAuthHeader(request) {
    if (!request.headers.authorization) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  private static async validateToken(auth: string) {
    if (auth.split(' ')[0] !== 'Bearer') {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }

    const token = auth.split(' ')[1];

    try {
      return await jwt.verify(token, process.env.SECRET);
    } catch (error) {
      const message = 'Invalid token: ' + (error.message || error.name);
      throw new HttpException(message, HttpStatus.UNAUTHORIZED);
    }
  }
}
